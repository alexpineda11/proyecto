<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Htpp\Controllers\TemperaturaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
  //  return $request->user();
//});

Route::post('register', 'App\Http\Controllers\UserController@register');
Route::post('login', 'App\Http\Controllers\UserController@authenticate');




Route::group(['middleware' => ['jwt.verify']], function() {

    Route::post('user','App\Http\Controllers\UserController@getAuthenticatedUser');


    //Temperatura
    Route::get('guardarTemperatura','App\Http\Controllers\TemperaturaController@index');
    Route::get('historialTemperatura','App\Http\Controllers\TemperaturaController@historial');
    Route::post('temperatura',[TemperaturaController::class,'store']);
    Route::get('temperatura/{id}',[TemperaturaController::class,'show']);
    Route::get('temperatura/{id}',[TemperaturaController::class,'update']);
    Route::get('temperatura/{id}',[TemperaturaController::class,'destroy']);

    //luz
    Route::get('guardarLuz','App\Http\Controllers\LuzController@index');
    Route::get('historialLuz','App\Http\Controllers\LuzController@historial');
    Route::post('luz',[LuzController::class,'store']);
    Route::get('luz/{id}',[LuzController::class,'show']);
    Route::get('luz/{id}',[LuzController::class,'update']);
    Route::get('luz/{id}',[LuzController::class,'destroy']);

     //humedad
     Route::get('guardarHumedad','App\Http\Controllers\HumedadController@index');
     Route::get('historialHumedad','App\Http\Controllers\HumedadController@historial');
     Route::post('humedad',[HumedadController::class,'store']);
     Route::get('humedad/{id}',[HumedadController::class,'show']);
     Route::get('humedad/{id}',[HumedadController::class,'update']);
     Route::get('humedad/{id}',[HumedadController::class,'destroy']);


       //boton
     Route::get('guardarBoton','App\Http\Controllers\BotonController@index');
     Route::get('historialBoton','App\Http\Controllers\BotonController@historial');
     Route::post('boton',[BotonController::class,'store']);
     Route::get('boton/{id}',[BotonController::class,'show']);
     Route::get('boton/{id}',[BotonController::class,'update']);
     Route::get('boton/{id}',[BotonController::class,'destroy']);
    
});
