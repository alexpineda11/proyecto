<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class humedad extends Model
{
    protected $table ="humedads";
     
    protected $fillable = [
    
        'humedad',
        
    ];

    protected $primarykey='id';
    public function getJWTIdentifier()
    {
    	return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
    	return [];
    }
}
