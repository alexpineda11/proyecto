<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class luz extends Model
{
    protected $table ="luzs";
     
    protected $fillable = [
    
        'luz',
        
    ];

    protected $primarykey='id';
    public function getJWTIdentifier()
    {
    	return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
    	return [];
    }
}
