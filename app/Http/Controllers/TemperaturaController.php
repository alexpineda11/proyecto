<?php

namespace App\Http\Controllers;

use App\Models\temperatura;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Illuminate\Support\Facades\Http;

class TemperaturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function historial(){
        return temperatura::all();
    }

    public function index()
    {
        $respuesta=HTTP::get('https://io.adafruit.com/api/v2/FabianOrtiz/feeds/temperatura/data/last?x-aio-key=aio_NZPb870hQ0iPSZ4Bj4lHSZvsai7C');
        $data=$respuesta->object();
            $model = new temperatura();
        
            $model->temperatura=$data->value;
           
            $model->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoretemperaturaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoretemperaturaRequest $request)
    {
        temperatura::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'temperatura guardada'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\temperatura  $temperatura
     * @return \Illuminate\Http\Response
     */
    public function show(temperatura $temperatura)
    {
        return response()->json([
            'res'=>true,
            'temperatura'=>$temperatura
        ],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\temperatura  $temperatura
     * @return \Illuminate\Http\Response
     */
    public function edit(temperatura $temperatura)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatetemperaturaRequest  $request
     * @param  \App\Models\temperatura  $temperatura
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatetemperaturaRequest $request, temperatura $temperatura)
    {
        $temperatura->update($temperatura->all());
        return response()->json([
            'res'=>true,
            'msg'=>'temperatura actualizada'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\temperatura  $temperatura
     * @return \Illuminate\Http\Response
     */
    public function destroy(temperatura $temperatura)
    {
        $temperatura->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'temperatura eliminada'
        ],200);
    }
}
